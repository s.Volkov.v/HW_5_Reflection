﻿
using System;
using System.Reflection;

namespace HomeWork_Reflection
{
    public static class MyHandler
    {

        public static string Serialize(object obj)
        {
            string result = String.Empty;
            Type objType = obj.GetType();
            foreach (FieldInfo field in objType.GetFields())
            {
                var value = field?.GetValue(obj);
                result += $"[{field?.Name}]({value})";
            }
            return result+";";
        }


        public static List<T> Deserialize<T>(string inputString)
        {
            List<T> listObj = new List<T>();
            var lstInputClass = new List<string>(inputString.Split(";"));
            int classCount = 0;
            foreach (string inputClass in lstInputClass)
            {
                classCount++;
                if (classCount == lstInputClass.Count) { break; }
                Type type = typeof(T);
                object? obj = Activator.CreateInstance(type);

                bool fieldNameStarted = false;
                bool fieldNameEnd = false;

                bool fieldValueStarted = false;
                bool fieldValueEnd = false;

                string fieldName = String.Empty;
                string fieldValue = String.Empty;

                int step = 0;
                foreach (char ch in inputClass)
                {
                    step++;

                    if (fieldNameStarted && fieldValueEnd &&
                        fieldValueStarted && fieldValueEnd)
                    {
                        FieldInfo? field = type.GetField(fieldName);
                        int fieldValueInt;
                        if (!int.TryParse(fieldValue, out fieldValueInt)) throw new Exception("Это не число");
                        field?.SetValue(obj, fieldValueInt);


                        fieldNameStarted = false;
                        fieldNameEnd = false;
                        fieldName = String.Empty;

                        fieldValueStarted = false;
                        fieldValueEnd = false;
                        fieldValue = String.Empty;
                    }


                    //Собираем имя
                    if (!fieldNameStarted && ch == '[')
                    {
                        fieldNameStarted = true;
                        continue;
                    }
                    if (!fieldNameEnd && ch == ']')
                    {
                        fieldNameEnd = true;
                        continue;
                    }
                    if (!fieldNameEnd)
                    {
                        fieldName += ch;
                        continue;
                    }

                    //Собираем значение
                   if (!fieldValueStarted && ch == '(')
                    {
                        fieldValueStarted = true;
                        continue;
                    }
                   if (!fieldValueEnd && ch == ')' )
                    {
                        fieldValueEnd = true;
                        if (step == inputClass.Length)
                        {
                            FieldInfo? field = type.GetField(fieldName);
                            int fieldValueInt;
                            if (!int.TryParse(fieldValue, out fieldValueInt)) throw new Exception("Это не число");
                            field?.SetValue(obj, fieldValueInt);
                        }
                        continue;
                    }
                   if (!fieldValueEnd)
                    {
                        fieldValue += ch;
                        continue;
                    }
                }
                if(obj!=null)
                    listObj.Add((T)obj);
            }

            return listObj;
        }


        }

}



    

