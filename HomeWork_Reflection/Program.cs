﻿using System.Diagnostics;
using System.Text.Json;
using HomeWork_Reflection;

//Сериализация и десерриализция через мой метод
var timer = new Stopwatch();
Console.WriteLine("----------");
Console.WriteLine("Сериализация и десерриализция через мой метод");
Console.WriteLine("Запуск таймера");
timer.Start();
Console.WriteLine("Запуск цикла");
for (int i = 0; i < 1000000; i++)
{
    F Fuu = new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    string serializedClassString = MyHandler.Serialize(Fuu);

    var result = MyHandler.Deserialize<F>(serializedClassString);
}
timer.Stop();
Console.WriteLine("Остановка таймера");
Console.WriteLine("Кол-во итераций 1000000");
TimeSpan span = timer.Elapsed;
Console.WriteLine($"Время заняло: {span.ToString(@"m\:ss\.fff")}");
Console.WriteLine("----------");
//

//Сериализация и десерриализция через JsonSerializer
var timer2 = new Stopwatch();
Console.WriteLine("----------");
Console.WriteLine("Сериализация и десерриализция через JsonSerializer");
Console.WriteLine("Запуск таймера");
timer2.Start();
Console.WriteLine("Запуск цикла");
var options = new JsonSerializerOptions { IncludeFields = true };
for (int i = 0; i < 1000000; i++)
{
    F fuu = new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    var json = JsonSerializer.Serialize(fuu, options);

    var jsonResult = JsonSerializer.Deserialize<F>(json, options);
}
timer2.Stop();
Console.WriteLine("Остановка таймера");
Console.WriteLine("Кол-во итераций 1000000");
TimeSpan span2 = timer2.Elapsed;
Console.WriteLine($"Время заняло: {span2.ToString(@"m\:ss\.fff")}");
Console.WriteLine("----------");
//


//Сериализация и десерриализция через мой метод в файл и обратно
var timer3 = new Stopwatch();
Console.WriteLine("----------");
Console.WriteLine("Сериализация и десерриализция через мой метод в файл и обратно");
Console.WriteLine("Запуск таймера");
timer3.Start();
Console.WriteLine("Запуск цикла");
using (StreamWriter sw = new StreamWriter("test.txt"))
{
    for (int i = 0; i < 1000000; i++)
    {
        F Fuu = new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
        string serializedClassString = MyHandler.Serialize(Fuu);
        sw.WriteLine(serializedClassString);
    }

}
using (StreamReader sr = new StreamReader("test.txt"))
{
    while (sr.ReadLine() is not null)
    {
        var result = MyHandler.Deserialize<F>(sr.ReadLine()??"");
    }
}


timer3.Stop();
Console.WriteLine("Остановка таймера");
Console.WriteLine("Кол-во итераций 1000000");
TimeSpan span3 = timer3.Elapsed;
Console.WriteLine($"Время заняло: {span3.ToString(@"m\:ss\.fff")}");
Console.WriteLine("----------");
//



Console.ReadLine();

